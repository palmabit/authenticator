<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Registrazione su FISM FAQ</h2>
<div>
  <p>Buongiorno,<br/>
      Non é possibile acquisire la sua richiesta di registrazione in quanto la scuola non risulta essere federata alla f.i.s.m.
      Per potersi federare e quindi accedere a tutti i servizi messi a disposizione é necessario rivolgersi alla propria fism provinciale.
      Può trovare tutti i riferimenti territoriali sul sito <a href="http://www.fism.net" target="_blank">www.fism.net</a> cliccando su “Fism in italia”.
	</p>
	<p>
     Cordiali Saluti
  </p>
</div>
</body>
</html>