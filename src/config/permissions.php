<?php

return [
    /**
     * Permission requirements to edit an user profile
     */
    "edit_profile" => ["_profile", "_superadmin"],
    /**
     * Group of users that gets notifications
     */
    "profile_notification_group" => "superadmin",
    /**
     * Permission requirements to access the admin area
     */
    "admin_area" => [''],
];